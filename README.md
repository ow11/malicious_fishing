# malicious_fishing

Homework

## How to install and use

1. Clone the repository with `git clone https://gitlab.com/ow11/malicious_fishing.git` or via ssh
2. Move to the repository folder with `cd malicious_fishing`
3. Create virtual environment with `virtaulenv venv`
4. Activate it with `source venv/bin/activate` (`venv\Scripts\activate` for Windows)
5. Install all requirements with `pip install -r requirements.txt`
6. Then you have to initialize the database with provided script `python initialize_db.py`
7. Now you can deploy it either run the development server

## How to run the flask development server

1. Define environment variable `export FLASK_APP=app.py` (`set FLASK_APP=app.py` for Windows)
2. Run the server with `flask run`

## How to browse the database

You can querry it with SQL commands or with ORM (e.g. SQLAlchemy ORM, which is used in the app)

**OR**

You can use a browser (e.g. [sqlitebrowser](https://sqlitebrowser.org/) or [sqlite-web](https://github.com/coleifer/sqlite-web))
